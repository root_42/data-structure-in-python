# Create a Singly Linked list
class Node:
    def __init__(self,element):
        self.data = element
        self.next = None
class Single_LinkedList:
    def __init__(self):
        self.head = None


if __name__ =="__main__":
    # Start with the empty list
    l_list = Single_LinkedList()
    l_list.head = Node(100)
    second_element = Node("Aman")
    third_element = Node("IIITM-Kerala")

    l_list.head.next = second_element
    second_element.next = third_element

print("printing the head of the linked list ",l_list.head.data)
            

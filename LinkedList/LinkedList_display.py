class Node:
    def __init__(self,data):
        self.data= data
        self.next = None

class LinkedList:
    def __init__(self):
        self.head = None


    def print_list(self):
        temp = self.head
        while(temp):
            print(temp.data)
            temp = temp.next


if __name__ == "__main__":
    l_list = LinkedList()
    l_list.head = Node(1001)
    second_element= Node(1002)
    third_element = Node(1003)

    l_list.head.next = second_element
    second_element.next = third_element
    l_list.print_list()
     


## Creating and displaying linked list where user inputs the things to insert in Linked List at head position

class Node:
    def __init__(self,data):
        self.data = data
        self.next = None


class LinkedList:
    def __init__(self):
        self.head = None


    def push(self,new_data):
        new_node = Node(new_data)
        new_node.next = self.head
        self.head = new_node

    def print_list(self):
        temp = self.head
        while(temp):
            print(temp.data)
            temp = temp.next

if __name__ == "__main__":
    l_list = LinkedList()
    user_input = list(map(int,input().split()))

    for elements in user_input:
       l_list.push(elements)
    l_list.print_list()
